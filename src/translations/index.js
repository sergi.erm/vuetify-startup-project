const files = require.context(
  '.', // current directory
  true, // subdirectories
  /.+\.json$/ // only .json
)
var messages = {}
files.keys().forEach(fileName => {
  messages[fileName.replace('.json', '').replace('./', '')] = files(fileName)
})
export default messages