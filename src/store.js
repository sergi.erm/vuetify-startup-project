import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

//almacén global de datos con vuex
const store = new Vuex.Store({
  state: {
    processing: false,
    language: 'es'
  },
  actions: {
    global_actions_changeLanguage: (context, lang) => {
      context.commit("globalTypes_mutations_setLanguage", lang);
    }
  },
  getters: {
    globalTypes_getters_language: state => state.language,
  },
  mutations: {
    globalTypes_mutations_startProcessing (state) {
      state.processing = true;
    },
    globalTypes_mutations_stopProcessing (state) {
      state.processing = false;
    },
    globalTypes_mutations_setLanguage (state, lang) {
      state.language = lang;
    }
  },
  /*modules: {
    authModule,
    cinemaModule,
    movieModule,
    bookingModule
}*/

//.almacén global de datos con vuex
})

export default store;