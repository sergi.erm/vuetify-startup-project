import Vue from 'vue'
import '@/plugins/vuetify'
import App from '@/App.vue'
import router from '@/router';
import store from '@/store';
import mixins from '@/mixin';


//vue traducciones
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);
import messages from '@/translations';
const i18n = new VueI18n({
  locale: store.state.language,
  messages
});
//.vue traducciones


Vue.config.productionTip = false
Vue.mixin(mixins);

new Vue({
  render: h => h(App),
  router,
  i18n,
  store  
}).$mount('#app')
