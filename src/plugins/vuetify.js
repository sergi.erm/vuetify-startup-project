import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary:  '#022865',
    secondary:'#1b98e0',
    success:  '#18A800',
    info:     '#78C0E0',
    error:    '#BA0000'
  }

})
