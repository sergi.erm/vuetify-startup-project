module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'es',
      fallbackLocale: 'en',
      localeDir: '@/translations',
      enableInSFC: undefined
    }
  }
}
